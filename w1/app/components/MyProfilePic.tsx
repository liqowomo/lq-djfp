import Image from "next/image"

export default function MyProfilePic() {
    return (
        <section className="w-full mx-auto">
            <Image
                className="border-4 border-red-800 dark:border-red-500 drop-shadow-xl shadow-red rounded-full mx-auto mt-8"
                src="https://i.imgur.com/aGFmbym.jpeg"
                width={200}
                height={200}
                alt="Liqo"
                priority={true}
            />
        </section>
    )
}